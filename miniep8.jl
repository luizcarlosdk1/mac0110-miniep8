function verificaValorCarta(x)
    v = TodasCartas()
    for i in 1:length(v)
        indice = i
        if x == v[i]
            return indice
        end
    end
end  

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
        for i in 2:tam
        j = i
        while j > 1
        if compareByValueAndSuit(v[j], v[j - 1]) == true  
         troca(v, j, j - 1)
        else 
            break
        end
    j = j - 1
        end
        end
    return v
end

function compareByValueAndSuit(x, y)
    x = verificaValorCarta(x)
    y = verificaValorCarta(y) 
    if x < y 
        return true 
    else 
        return false 
    end 
end

function TodasCartas()
    allcards = []
    numeros = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"]
    for i in 1:length(numeros) 
        card = numeros[i] * "♢"
        push!(allcards,card)
    end
    for i in 1:length(numeros) 
        card = numeros[i] * "♠"
        push!(allcards,card)
    end
    for i in 1:length(numeros) 
        card = numeros[i] * "♡"
        push!(allcards,card)
    end
    for i in 1:length(numeros) 
        card = numeros[i] * "♣"
        push!(allcards,card)
    end
    return allcards
end

using Test

function test()
    @test compareByValueAndSuit("2♠","A♠") == true 
    @test compareByValueAndSuit("K♡","10♡") == false
    @test compareByValueAndSuit("10♠","10♡") == true 
    @test compareByValueAndSuit("A♠","2♡") == true
    @test insercao(["10♡","10♢","K♠","A♠","J♠","A♠"]) == ["10♢","J♠","K♠","A♠","A♠","10♡"] 
    end
    
test()